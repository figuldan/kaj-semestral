import { React, render, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import App from '../App'
import { MemoryRouter } from 'react-router-dom'

test('Home component renders', () => {
  render(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  )
  const linkElement = screen.getByText(/Your favorite quiz application/)
  expect(linkElement).toBeInTheDocument()
})
