import React, { useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import './Create.css'

const Create = () => {
  const getNewGeneralQuiz = () => ({ id: uuidv4(), name: '', description: '', questions: [] })
  const getNewGeneralAnswer = () => ({ answer: '', correct: false })
  const getNewGeneralQuestion = () => ({ question: '', answers: [{ answer: '', correct: true }] })

  const [quiz, setQuiz] = useState(getNewGeneralQuiz())
  const [questions, setQuestions] = useState([getNewGeneralQuestion()])
  const [showSuccessToast, setShowSuccessToast] = useState(false)
  const [showIncompleteToast, setShowIncompleteToast] = useState(false)
  const [, setResetForm] = useState(false)

  const addAnswer = (questionIndex) => {
    setQuestions((prevQuestions) => {
      return prevQuestions.map((question, index) => {
        if (index === questionIndex) {
          return { ...question, answers: [...question.answers, getNewGeneralAnswer()] }
        }
        return question
      })
    })
  }

  const addQuestion = () => {
    setQuestions([...questions, getNewGeneralQuestion()])
  }

  const handleQuestionChange = (e, questionIndex) => {
    setQuestions(
      questions.map((question, index) =>
        index === questionIndex ? { ...question, question: e.target.value } : question
      )
    )
  }

  const handleDescriptionChange = (e) => {
    setQuiz({ ...quiz, description: e.target.value })
  }

  const handleAnswerChange = (e, questionIndex, answerIndex) => {
    setQuestions(
      questions.map((question, qIndex) =>
        qIndex === questionIndex
          ? {
              ...question,
              answers: question.answers.map((answer, aIndex) =>
                aIndex === answerIndex ? { ...answer, answer: e.target.value } : answer
              )
            }
          : question
      )
    )
  }

  const handleAnswerCorrectChange = (questionIndex, answerIndex) => {
    setQuestions(
      questions.map((question, qIndex) =>
        qIndex === questionIndex
          ? {
              ...question,
              answers: question.answers.map((answer, aIndex) =>
                aIndex === answerIndex ? { ...answer, correct: true } : { ...answer, correct: false }
              )
            }
          : question
      )
    )
  }

  const validateQuiz = () => {
    if (!quiz.name || !quiz.description) return false
    for (const question of questions) {
      if (!question.question) return false
      for (const answer of question.answers) {
        if (!answer.answer) return false
      }
    }
    return true
  }

  const saveQuizToLocalStorage = () => {
    if (validateQuiz()) {
      const updatedQuiz = { ...quiz, questions }
      localStorage.setItem('quiz-' + updatedQuiz.id, JSON.stringify(updatedQuiz))
      setShowSuccessToast(true)
      setResetForm(true)
      setQuiz(getNewGeneralQuiz())
      setQuestions([getNewGeneralQuestion()])
    } else {
      setShowIncompleteToast(true)
      setTimeout(() => setShowIncompleteToast(false), 3000)
    }
  }

  const handleFormSubmit = (e) => {
    e.preventDefault()
    saveQuizToLocalStorage()
  }

  return (
    <div className="main-container">
      {showSuccessToast && <div className="toast success">Quiz saved successfully!</div>}
      {showIncompleteToast && <div className="toast incomplete">Fill out all of the inputs</div>}
      <form onSubmit={handleFormSubmit}>
        <fieldset>
          <legend>Create New Quiz</legend>
          <label htmlFor="quizName">Name</label>
          <br />
          <input
            type="text"
            id="quizName"
            name="quizName"
            placeholder="Quiz about horses"
            autoFocus
            value={quiz.name}
            onChange={(e) => setQuiz({ ...quiz, name: e.target.value })}
          />
          <label htmlFor="quizDescription">Description</label>
          <br />
          <input
            type="text"
            id="quizDescription"
            name="quizDescription"
            placeholder="Description"
            value={quiz.description}
            onChange={(e) => handleDescriptionChange(e)}
          />
          {questions.map((question, questionIndex) => (
            <div key={questionIndex} className="question-wrapper">
              <label htmlFor={`question-${questionIndex}`}>Question</label>
              <br />
              <input
                type="text"
                id={`question-${questionIndex}`}
                name={`question-${questionIndex}`}
                placeholder='What would you like to ask?'
                value={question.question}
                onChange={(e) => handleQuestionChange(e, questionIndex)}
              />
              <fieldset>
                <legend>Select correct answer</legend>
                {question.answers.map((answer, answerIndex) => (
                  <div key={answerIndex} className="answer-wrapper">
                    <label htmlFor={`answer-${questionIndex}-${answerIndex}`}>Answer</label>{' '}
                    <input
                      type="text"
                      id={`answer-${questionIndex}-${answerIndex}`}
                      name={`answer-${questionIndex}-${answerIndex}`}
                      placeholder='What would be the answer?'
                      value={answer.answer}
                      onChange={(e) => handleAnswerChange(e, questionIndex, answerIndex)}
                    />
                    <input
                      type="radio"
                      id={`answer-correct-${questionIndex}-${answerIndex}`}
                      name={`answer-correct-${questionIndex}`}
                      checked={answer.correct}
                      onChange={() => handleAnswerCorrectChange(questionIndex, answerIndex)}
                    />
                    Correct?
                  </div>
                ))}
              </fieldset>
              <button type="button" className="add-btn" onClick={() => addAnswer(questionIndex)}>
                Add Answer
              </button>
            </div>
          ))}
        </fieldset>
        <button type="button" className="add-btn" onClick={addQuestion}>
          Add Question
        </button>
        <button type="submit" className="save-btn">
          Create quiz
        </button>
      </form>
    </div>
  )
}

export default Create
