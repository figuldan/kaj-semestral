import React from 'react'
import { useParams, useNavigate } from 'react-router-dom'

const QuizOverview = () => {
  const { cardId } = useParams()
  const navigate = useNavigate()

  const quizData = JSON.parse(localStorage.getItem(cardId))

  return (
    <div>
      <h1>Quiz Summary</h1>
      <ul>
        {quizData.questions.map((question, index) => (
          <li key={index}>
            <strong>Question {index + 1}:</strong> {question.question}
            <br />
            <strong>Correct Answer:</strong>{' '}
            {question.answers.find((ans) => ans.correct).answer}
            <p>Your answer: {quizData[index]}</p>
          </li>
        ))}
      </ul>
      <button onClick={() => navigate(`/play/${cardId}`)}>
        Return to Quiz
      </button>
    </div>
  )
}

export default QuizOverview
