import { useParams, Link, useNavigate } from 'react-router-dom'
import { useEffect, useState } from 'react'
import './QuizDetail.css'

const QuizDetail = () => {
  const { cardId } = useParams()
  const navigate = useNavigate()
  const [quiz, setQuiz] = useState(null)

  useEffect(() => {
    const storedQuiz = JSON.parse(localStorage.getItem(`quiz-${cardId}`))
    if (storedQuiz) {
      setQuiz(storedQuiz)
    } else {
      navigate('/not-found') // Redirect if quiz not found
    }

    window.history.pushState({ page: 'quizDetail' }, '')

    const handlePopState = (event) => {
      if (event.state && event.state.page === 'quizDetail') {
        navigate('/play')
      }
    }

    window.addEventListener('popstate', handlePopState)

    return () => {
      window.removeEventListener('popstate', handlePopState)
    }
  }, [cardId, navigate])

  if (!quiz) {
    return <div>Loading...</div>
  }

  return (
    <>
      <header className="quiz-detail-header">
        <h1 className="quiz-name">{quiz.name}</h1>
        <h2 className="quiz-description">{quiz.description}</h2>
        <h3>Questions:</h3>
      </header>
      <ol className="quiz-questions">
        {quiz.questions.map((question, index) => (
          <li className="quiz-question" key={index}>{question.question}</li>
        ))}
      </ol>
      <Link to='0' className="quiz-button">Play</Link>
    </>
  )
}

export default QuizDetail
