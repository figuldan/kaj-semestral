import React, { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import WarningModal from '../../components/WarningModal/WarningModal'
import './QuizQuestion.css'

const QuizQuestion = () => {
  const { cardId, questionId } = useParams()
  const navigate = useNavigate()

  const quizData = JSON.parse(localStorage.getItem('quiz-' + cardId))
  const question = quizData.questions[questionId]

  const [selectedAnswer, setSelectedAnswer] = useState(null)
  const [result, setResult] = useState(null)
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [isConfirmed, setIsConfirmed] = useState(false)
  const [attemptData, setAttemptData] = useState(null)

  useEffect(() => {
    const attemptId = `attempt_${new Date().getTime()}_${Math.floor(Math.random() * 1000)}`
    const newAttemptData = {
      datetime: new Date(),
      attemptId,
      quizId: quizData.id, // Correctly setting quizId
      answers: []
    }
    setAttemptData(newAttemptData)

    const handleBackButton = (event) => {
      event.preventDefault()
      setIsModalOpen(true)
    }

    window.history.pushState(null, '', window.location.href)
    window.addEventListener('popstate', handleBackButton)

    return () => {
      window.removeEventListener('popstate', handleBackButton)
    }
  }, [quizData.id])

  const handleSubmit = (event) => {
    event.preventDefault()
    const correctAnswer = question.answers.find((ans) => ans.correct)
    const questionName = question.question
    const answerData = {
      questionName,
      questionId,
      selectedAnswer,
      correctAnswer: correctAnswer.answer
    }
    setAttemptData((prevState) => ({
      ...prevState,
      answers: [...prevState.answers, answerData]
    }))
    setSelectedAnswer(null)
    setResult(selectedAnswer === correctAnswer.answer ? '✅ Correct!' : '❌ Incorrect')
    // Disable radio buttons after submission
    const radioButtons = document.getElementsByName('answer')
    radioButtons.forEach((button) => {
      button.disabled = true
    })
  }

  const handleNext = () => {
    // Check if this is the last question
    if (parseInt(questionId, 10) === quizData.questions.length - 1) {
      localStorage.setItem(`Attempts-${quizData.id}-${attemptData.attemptId}`, JSON.stringify(attemptData))
      navigate(`/finish/${quizData.id}/${attemptData.attemptId}`)
    } else {
      setResult(null)
      setSelectedAnswer(null)
      const nextQuestionId = parseInt(questionId, 10) + 1
      navigate(`/play/${cardId}/${nextQuestionId}`)
    }
  }

  const handleConfirm = () => {
    setIsConfirmed(true)
    setIsModalOpen(false)
  }

  const handleCancel = () => {
    window.history.pushState(null, '', window.location.href)
    setIsModalOpen(false)
  }

  useEffect(() => {
    if (isConfirmed) {
      navigate(`/play/${cardId}`)
    }
  }, [isConfirmed, navigate, cardId])

  return (
    <div className="quiz-question-container">
      <h1 className="quiz-question">{question.question}</h1>
      <form onSubmit={handleSubmit}>
        <ul className="quiz-answer-list">
          {question.answers.map((ans, index) => (
            <li className="quiz-answer-item" key={index}>
              <label className="quiz-answer-label">
                <input
                  type="radio"
                  name="answer"
                  value={ans.answer}
                  onChange={() => setSelectedAnswer(ans.answer)}
                  checked={selectedAnswer === ans.answer}
                  disabled={result !== null} // Disable if result is not null
                />
                {ans.answer} {/* Option text */}
              </label>
            </li>
          ))}
        </ul>
        <button className="quiz-submit-button" type="submit" disabled={!selectedAnswer || result !== null}>
          Submit
        </button>
        {result && <p className="quiz-result">{result}</p>}
        {result && (
          <button className="quiz-next-button" type="button" onClick={handleNext}>
            Next
          </button>
        )}
      </form>
      <WarningModal
        isOpen={isModalOpen}
        onConfirm={handleConfirm}
        onCancel={handleCancel}
      />
    </div>
  )
}

export default QuizQuestion
