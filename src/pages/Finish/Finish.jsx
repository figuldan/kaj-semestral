import React from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import './Finish.css'

const QuizFinish = () => {
  const { cardId, attemptId } = useParams()
  const navigate = useNavigate()

  const attemptData = JSON.parse(localStorage.getItem(`Attempts-${cardId}-${attemptId}`))

  return (
    <div className="quiz-finish-container">
      <h1 className="quiz-finish-title">Quiz Summary</h1>
      <ul className="quiz-finish-list">
        {attemptData && attemptData.answers.map((answerData, index) => (
          <li className="quiz-finish-item" key={index}>
            <h3 className="quiz-finish-question">{answerData.questionName}</h3>
            {answerData.selectedAnswer === answerData.correctAnswer
              ? (
              <p className="quiz-finish-correct">{answerData.selectedAnswer}</p>
                )
              : (
              <>
                <p className="quiz-finish-wrong">{answerData.selectedAnswer}</p>
                <p className="quiz-finish-correct">Correct Answer: {answerData.correctAnswer}</p>
              </>
                )}
          </li>
        ))}
      </ul>
      <button className="quiz-finish-button" onClick={() => navigate(`/play/${cardId}`)}>
        Return to Quiz
      </button>
    </div>
  )
}

export default QuizFinish
