import { Link } from 'react-router-dom'
import './Landing.css'

const Landing = () => (
  <>
    <section className="landing-wrapper">
      <header className="landing-title-wrapper">
        <hgroup>
          <h1 className="landing-title">QuizZen</h1>
          <h3 className="landing-description">
            Your favorite quiz application
          </h3>
        </hgroup>
        <p>
          Welcome to QuizZen - the ultimate destination for creating and playing
          quizzes that will challenge your mind and sharpen your knowledge!
        </p>
      </header>
      <nav className="main-nav">
        <ul className="main-nav-wrapper">
          <li className="main-nav-link">
            <Link to={'/play'}>Play</Link>
          </li>
          <li className="main-nav-link">
            <Link to={'/create'}>Create</Link>
          </li>
        </ul>
      </nav>
    </section>
  </>
)

export default Landing
