/* eslint-disable multiline-ternary */
import React from 'react'
import { Link } from 'react-router-dom'
import Quiz from '../../components/Card/Card'
import './Play.css'

const Play = () => {
  const localStorageEntries = Object.entries(localStorage)

  const quizEntries = localStorageEntries.filter(([key, value]) => {
    try {
      const parsedValue = JSON.parse(value)
      return (
        parsedValue &&
        parsedValue.id &&
        parsedValue.name &&
        parsedValue.description &&
        Array.isArray(parsedValue.questions)
      )
    } catch (e) {
      return false
    }
  })

  return (
    <>
      <div className="quiz-wrapper">
        {quizEntries.length === 0 ? (
          <div className="no-quizzes">
            <p><b>No quizzes are present.</b></p>
            <Link to="/create">
              <button>Create Quiz</button>
            </Link>
          </div>
        ) : (
          quizEntries.map(([id, card]) => {
            const quiz = JSON.parse(card)
            return (
              <div className="quiz-card" key={id}>
                <Quiz
                  id={quiz.id}
                  name={quiz.name}
                  description={quiz.description}
                />
              </div>
            )
          })
        )}
      </div>
    </>
  )
}

export default Play
