import { Routes, Route } from 'react-router-dom'
import Landing from '../../pages/Landing/Landing'
import Create from '../../pages/Create/Create'
import Play from '../../pages/Play/Play'
import PlayQuiz from '../../pages/QuizDetail/QuizDetail'
import QuizQuestion from '../../pages/QuizQuestion/QuizQuestion'
import QuizFinish from '../../pages/Finish/Finish'
import QuizOverview from '../../pages/Overview/Overview'

const Router = () => (
  <>
    <Routes>
      <Route path="/" element={<Landing />} />
      <Route path="/play" element={<Play />} />
      <Route path="/play/:cardId" element={<PlayQuiz />} />
      <Route path="/create" element={<Create />} />
      <Route path="/play/:cardId/:questionId" element={<QuizQuestion />} />
      <Route path="/finish/:cardId" element={<QuizOverview />} />
      <Route path="/finish/:cardId/:attemptId" element={<QuizFinish />} />
    </Routes>
  </>
)

export default Router
