import React, { useEffect } from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import './WarningModal.css'

const WarningModal = ({ isOpen, onConfirm, onCancel }) => {
  useEffect(() => {
    Modal.setAppElement('#root')
  }, [])

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onCancel}
      contentLabel="Warning"
      className="modal"
      overlayClassName="overlay"
    >
      <h2>Warning</h2>
      <p>You are about to leave this page. Do you want to proceed?</p>
      <div className="button-container">
        <button onClick={onConfirm} className="confirm-button">Yes</button>
        <button onClick={onCancel} className="cancel-button">No</button>
      </div>
    </Modal>
  )
}

WarningModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired
}

export default WarningModal
