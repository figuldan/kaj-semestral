import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import '../../pages/Play/Play.css'

const Card = ({ id, name, description }) => {
  return (
    <div key={id}>
      <h4>{name}</h4>
      <p>{description}</p>
      <Link className='quiz-button' to={`/play/${id}`}>Details</Link>
    </div>
  )
}

Card.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default Card
