import React from 'react'
import { Link } from 'react-router-dom'
import './NavigationBar.css'

const QuestionMarkIcon = () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" className="logo-icon">
    <rect width="100%" height="80%" fill="none" />
    <circle cx="50" cy="40" r="40" fill="#ffffff" />
    <text x="35" y="57" fontFamily="Arial" fontSize="50" fill="#000000">?</text>
  </svg>
)

const NavigationBar = () => (
  <nav className="nav">
    <ul className="nav-bar">
      <li className="nav-title">
        <Link to={'/'}>
          <span className="brand-logo">
            <QuestionMarkIcon />QuizZen</span>
        </Link>
      </li>
      <li className="nav-link">
        <Link to={'/play'}>
          Play
        </Link>
      </li>
      <li className="nav-link">
        <Link to={'/create'}>
          Create Quiz
        </Link>
      </li>
    </ul>
  </nav>
)

export default NavigationBar
