import NavigationBar from './components/NavigationBar/NavigationBar'
import Router from './components/Router/Router'

const App = () => (
  <>
    <NavigationBar />
    <Router />
  </>
)

export default App
