# QuizZen

✍️ **Autor:** *Daniel Figuli*

📌 **URL:** <https://figuldan.pages.fel.cvut.cz/kaj-semestral/>

## 📝 Popis aplikácie

*QuizZen* je webová aplikácia na vytváranie a prehrávanie jednoduchých quizov. Užívateľ si tak vie pomocou dynamického formulára vytvoriť quiz s N otázkami a M odpoveďami na ne, v rámci ktorých je jediná možnosť správna.

![Landing Page](/public/documentation/landing_page.png)
*Obrázok č. 1: Domovská stránka*

## 🛠️ Použité technológie

Aplikáca je postavená na jazykoch **HTML5**, **CSS3**, **Javascript** a Javascriptovom frameworku **React**.

V rámci React frameworku boli využité následovné knižnice:

* [React Router](https://reactrouter.com/en/main): umožňuje spravovanie navigácie a smerovanie v aplikácii
* [React Modal](http://reactcommunity.org/react-modal/): komponenty pre zobrazovanie modálnych okien
* [uuidv4](https://github.com/thenativeweb/uuidv4/): na generovanie unikátných identifikátorov rámci ukladania dát
* [eslint](https://eslint.org/): pre statickú analýzu kódu, v ktorej konvencie sú zadefinované v súbore [.eslintrc.json](.eslintrc.json) v koreňovom adresári

Taktiež pre vývoj bol použitý [Gitlab CI](.gitlab-ci.yml) pre automatizáciu buildovania, testovania, statickej analýzy kódu a následné nasadenie na [Gitlab Pages](https://figuldan.pages.fel.cvut.cz/kaj-semestral/).

## 📱 Funkcionality aplikácie

Webová aplikácia poskytuje následujúce funkcionality:

### ✏️ Vytváranie quizov

Užívateľ si vie vytvoriť quiz s otázkami, v rámci ktorých vie označiť ako jednu správnu

![Create Page](/public/documentation/create_page.png)
*Obrázok č. 2: Stránka pre vytváranie quizov*

### 📋 Zobrazovanie quizov

Užívateľ si vie vytvorené quizy zobraziť a prezerať obsah quizu. Pozrieť si správne odpovede otázok zvoleného quizu nie je povolené

![Quiz Detail Page](/public/documentation/quiz_detail_page.png)
*Obrázok č. 3: Detail vytvoreného quizu*

### 🎮 Prehrávanie quizov

Užívateľ si daný quiz vie prehrať a následne tak aj pozrieť jeho výsledky vďaka jednoduchej štatistike na konci

![Quiz Statistics Page](/public/documentation/quiz_statistics_page.png)
*Obrázok č. 4: Výsledná štatistika prehraného quizu*

## ✅ Kritéria ku splneniu semestrálnej práce

| **Kategorie** | **Popis** | **Povinné** | **Body** | **Splnené** |
|----------|----------|----------|----------|----------|
| **Dokumentace**    | | | **1** | |
| | Cíl projektu, postup, popis funkčnosti, komentáře ve zdrojovém kódu   | **X** | 1 | ✅ Toto [README](README.md) |
| | | | | |
| **HTML5**    |    |    | **10** |  |
| *Validita* | Validní použití HTML5 doctype | **X** | 1 | ✅ Kontrolované pomocou ESLint (viz [pipelines](https://gitlab.fel.cvut.cz/figuldan/kaj-semestral/-/pipelines)) |
| *Validita* | Fungující v moderních prohlíčečích v posledních vývojových verzích (Chrome, Firefox, Edge, Opera) | | 2 | ✅ Vyskúšané na posledných verziach |
| *Semantické značky* | Správné použití sémantických značek (section, article, nav, aside, ...) | **X** | 1 | ✅ Použité značky ako *main*, *nav*, *header*... |
| *Grafika - SVG / Canvas* | | | 2 | ✅ SVG použité len ako logo v navigačnej lište, Canvas nebol vhodný pre tento projekt |
| *Média - Audio/Video* | | | 2 | ❓ Nebolo vhodné pre tento projekt |
| *Formulářové prvky* | Validace, typy, placeholder, autofocus | | 2 | ✅ Viz vytváranie quizov |
| *Offline aplikace* | Využití možnosti fungování stránky bez Internetového připojení (viz sekce Javascript) | | 1 |  |
| | | | | |
| **CSS** | | | **8**  | |
| *Pokročilé selektory* | použití pokročilých pseudotříd a kombinátorů | **X** | 1 | |
| *Vendor prefixy* | | | 1 | |
| *CSS3 transformace 2D/3D* | | | 2 | ✅ Využité na viacerých stránkach projektu |
| *CSS3 transitions/animations* | | **X** | 2 | ✅ Využité na viacerých stránkach projektu |
| *Media queries* | Stránky fungují i na mobilních zařízeních i jiných (tedy nerozpadají se) | | 2 | ✅ `@media` použitý len na navigačnú lištu stránky, celkovo boli použité responzívne prvky |
| | | | | |
| **Javascript** | | | **12** | |
| *OOP přístup* | Prototypová dědičnost, její využití, jmenné prostory | **X** | | |
| *Použití JS frameworku či knihovny* | Použití a pochopení frameworku či knihovny jQuery, React, Vue .. | | 1 |  |
| *Použití pokročilých JS API* | Využití pokročilých API (File API, Geolocation, Drag & Drop, LocalStorage, Sockety, ...) | **X** | 3 | ✅ Využitý LocalStorage pre ukladanie quizov a zaznamenaných odpovedí |
| *Funkční historie* | Posun tlačítky zpět/vpřed prohlížeče - pokud to vyplývá z funkcionatilty (History API) | | 2 | ✅ Použitá knižnica React Router pre navigáciu ale ako aj History API pre manipulovanie konkretnej navigácie v niektorých situáciach |
| *Ovládání medií*| Použití Média API (video, zvuk), přehrávání z JS | | 1 | ❓ Nebolo vohdné pre tento projekt |
| *Offline aplikace* | Využití JS API pro zjišťování stavu | | 1 | |
| *JS práce se SVG* | Události, tvorba, úpravy | | 2 | |
| | | | | |
| **Ostatní** | | | **5** | |
| *Kompletnost řešení* | | | 3 | |
| *Estetické zpracování* | | | 2 | |
| *Pozdní odevzdání* | | | 0 | |
| | | | | |
| **CELKEM** | | | **36** | |
